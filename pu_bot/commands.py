""" Register commands and views for the bot. """

import logging
import hikari
import lightbulb
import miru

from pu_bot.auction_distributor import (
    AUCTIONS,
    Auction,
    complete_auction,
    create_auction,
    create_bid,
    save_auctions
)
from pu_bot.utils import get_username

from .bot import bot

LOGGER = logging.getLogger(__name__)

class BidButton(miru.Button):
    def __init__(self, auction: Auction):
        super().__init__(style=hikari.ButtonStyle.PRIMARY, label="Bid!", custom_id="bid_button_" + auction.id)
        self.auction = auction

    async def callback(self, ctx: miru.ViewContext) -> None:
        """Create a new bid and update the message."""
        try:
            username = await get_username(ctx.guild_id, ctx.user.id)
            create_bid(ctx.user.id, username, self.auction.id)
        except ValueError as error:
            await ctx.respond(content=str(error), flags=hikari.MessageFlag.EPHEMERAL)
            return

        await self.view.update_active_message()

class FinishButton(miru.Button):
    def __init__(self, auction: Auction):
        super().__init__(style=hikari.ButtonStyle.SUCCESS, label="Finish", custom_id="finish_button_" + auction.id)
        self.auction = auction

    async def callback(self, ctx: miru.ViewContext) -> None:
        """Finish out the auction attached to this view."""
        if ctx.user.id != self.auction.creator_id:
            await ctx.respond(
                content="Only the creator of an auction can finish it",
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            return

        await self.view.complete_auction()

class AuctionView(miru.View):
    """A view for the context."""

    def __init__(self, auction: Auction, *args, **kwargs) -> None:
        kwargs["timeout"] = None
        super().__init__(*args, **kwargs)

        self.auction = auction
        self.add_item(BidButton(auction))
        self.add_item(FinishButton(auction))

    async def update_active_message(self, message_owned: bool = True) -> str:
        new_message = f"{self.auction.amount} {self.auction.item}@{self.auction.price:.2f} {self.auction.currency}/u available on {self.auction.location}"
        if len(self.auction.bids) > 0:
            new_message += "\nBids:"
        for bid in self.auction.bids:
            new_message += f"\n* {bid.username}"

        if message_owned:
            await self.message.edit(content=new_message)
            self.message.content = new_message

        return new_message

    async def complete_auction(self) -> None:
        """Close out the auction and display the results."""
        message = await complete_auction(self.auction)
        assert self.message is not None
        await self.message.edit(content=message, components=[])
        self.stop()

    async def on_timeout(self) -> None:
        """Close out the auction after a period of inactivity"""

        await self.complete_auction()

@bot.listen()
async def startup_views(event: hikari.StartedEvent) -> None:
    for auction_id in AUCTIONS.keys():
        view = AuctionView(AUCTIONS[auction_id])
        message = await event.app.rest.fetch_message(AUCTIONS[auction_id].channel_id, AUCTIONS[auction_id].msg_id)
        await view.start(message)

@bot.command
@lightbulb.command("ping", "Check if the bot is alive")
@lightbulb.implements(lightbulb.SlashCommand)
async def ping(ctx: lightbulb.Context) -> None:
    """Simple ping handler to test bot functionality"""

    await ctx.respond("Pong!")


@bot.command
@lightbulb.option(
    name="item",
    type=str,
    description="The item you want to create an auction for",
    required=True,
)
@lightbulb.option(
    name="amount",
    type=int,
    description="The amount of items you want to auction off",
    required=True,
)
@lightbulb.option(
    name="price",
    type=float,
    description="The price of each item you want to auction off",
    required=True,
)
@lightbulb.option(
    name="location",
    type=str,
    description="The location of the auction.",
    required=True,
)
@lightbulb.option(
    name="currency",
    type=str,
    description="The currency to use for the auction.",
    default="AIC",
    required=False,
)
@lightbulb.command("auction", "Start a new auction")
@lightbulb.implements(lightbulb.SlashCommand)
async def create_new_auction(ctx: lightbulb.Context) -> None:
    """Create a new auction for an item."""

    auction = create_auction(
        guild_id=ctx.guild_id,
        user_id=ctx.user.id,
        item=ctx.options.item,
        amount=ctx.options.amount,
        price=ctx.options.price,
        location=ctx.options.location,
        currency=ctx.options.currency,
    )

    view = AuctionView(auction)

    message = await ctx.respond(
        await view.update_active_message(False),
        components=view,
    )

    msg = await message.message()
    auction.msg_id = msg.id
    auction.channel_id = msg.channel_id
    save_auctions()

    await view.start(message)
    await view.wait()
