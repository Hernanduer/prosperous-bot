"""Run the bot."""

from pu_bot.bot import bot

import pu_bot.commands  # pylint: disable=unused-import
from pu_bot.auction_distributor import load_auctions

load_auctions()

bot.run()
