"""Track auctions that allow equal distribution and sale of materials."""

import json

from typing import Optional
from uuid import uuid4
from hikari import Snowflake
from pydantic import BaseModel, Field, validate_arguments

from pu_bot.utils import get_username
from pu_bot.bot import SETTINGS

# TODO: Persist this with a database (or at least a file) later
AUCTIONS: dict[str, "Auction"] = {}


class Auction(BaseModel):
    """A single auction for an item."""

    msg_id: Snowflake = None
    channel_id: Snowflake = None
    guild_id: Snowflake
    creator_id: Snowflake
    id: str = Field(default_factory=lambda: str(uuid4()))
    item: str
    amount: int
    price: float
    location: str
    currency: str
    bids: list["Bid"] = []

    def to_map(self) -> str:
        return {
            'msg_id': self.msg_id,
            'channel_id': self.channel_id,
            'guild_id': self.guild_id,
            'creator_id': self.creator_id,
            'id': self.id,
            'item': self.item,
            'amount': self.amount,
            'price': self.price,
            'location': self.location,
            'currency': self.currency,
            'bids': [ {
                'user_id': i.user_id,
                'username': i.username,
                'auction_id': i.auction_id,
                'max_amount': i.max_amount
                       } for i in self.bids ]
        }


class Bid(BaseModel):
    """A bid on an auction"""

    user_id: Snowflake
    username: str
    auction_id: str
    max_amount: Optional[int]

    @property
    def auction(self) -> Auction:
        """Get the auction for this bid."""
        return AUCTIONS[self.auction_id]


AuctionResult = dict[Snowflake, int]

def save_auctions() -> None:
    """Store off the current auctions map"""
    save_map = {}
    for auction_id in AUCTIONS.keys():
        save_map[auction_id] = AUCTIONS[auction_id].to_map()
    with open(SETTINGS.savefile, 'w') as savefile:
        savefile.write(json.dumps(save_map))

def load_auctions():
    """Load any active auctions from the save file"""
    auctions = {}
    with open(SETTINGS.savefile, 'r') as savefile:
        json_str = savefile.read()
        if len(json_str) > 1:
            auctions = json.loads(json_str)

    for auction_id in auctions:
        auction = auctions[auction_id]
        print("Loading auction ", auction)
        AUCTIONS[auction_id] = Auction(
            msg_id=auction['msg_id'],
            channel_id=auction['channel_id'],
            guild_id=auction['guild_id'],
            creator_id=auction['creator_id'],
            id=auction['id'],
            item=auction['item'],
            amount=auction['amount'],
            price=auction['price'],
            location=auction['location'],
            currency=auction['currency']
        )
        for bid in auction['bids']:
            AUCTIONS[auction_id].bids.append(Bid(user_id=bid['user_id'],
                                                 username=bid['username'],
                                                 auction_id=bid['auction_id'],
                                                 max_amount=bid['max_amount']))


@validate_arguments
def create_auction(
    guild_id: Snowflake, user_id: Snowflake, item: str, amount: int, price: float, location: str, currency: str
) -> Auction:
    """Start an auction"""

    auction = Auction(
        guild_id=guild_id, creator_id=user_id, item=item, amount=amount, price=price, location=location, currency=currency
    )

    assert auction.id not in AUCTIONS
    AUCTIONS[auction.id] = auction
    save_auctions()

    return auction


@validate_arguments
def create_bid(
    user_id: Snowflake, username: str, auction_id: str, max_amount: Optional[int] = None
) -> Bid:
    """Create a new bid for an auction"""

    if auction_id not in AUCTIONS:
        raise ValueError(f"Auction {auction_id} does not exist")

    auction = AUCTIONS[auction_id]

    for bid in auction.bids:
        if user_id == bid.user_id:
            raise ValueError("You can bid only an auction once")

    bid = Bid(user_id=user_id, username=username, auction_id=auction_id, max_amount=max_amount)

    bid.auction.bids.append(bid)
    save_auctions()

    return bid


def calculate_auction_results(user_id: Snowflake, auction_id: str) -> AuctionResult:
    """Finish an auction and calculate distribution of items to the bidders."""

    if auction_id not in AUCTIONS:
        raise ValueError(f"Auction {auction_id} does not exist")

    auction: Auction = AUCTIONS[auction_id]

    if user_id != auction.creator_id:
        raise ValueError("Only the creator of an auction can finish it")

    max_amount_per = auction.amount // (len(auction.bids) or 1)
    auction_results: AuctionResult = {}

    resolved = False
    while not resolved:
        resolved = True
        for bid in auction.bids:
            if bid.user_id in auction_results:
                # Max amount already hit and value distributed
                continue

            if bid.max_amount and bid.max_amount < max_amount_per:
                resolved = False
                auction_results[bid.user_id] = bid.max_amount
                try:
                    max_amount_per += (max_amount_per - bid.max_amount) // (
                        len(auction.bids) - len(auction_results)
                    )
                except ZeroDivisionError:
                    # All bids hit max threshold, no more bids to process
                    resolved = True

    # No max amount thresholds breached, divide out the rest evenly
    for bid in auction.bids:
        if bid.user_id in auction_results:
            continue

        auction_results[bid.user_id] = max_amount_per

    # Sanity check the results
    assert sum(auction_results.values()) <= auction.amount
    for bid in auction.bids:
        assert bid.user_id in auction_results
        if bid.max_amount:
            assert auction_results[bid.user_id] <= bid.max_amount
        assert auction_results[bid.user_id] <= max_amount_per

    return auction_results


async def complete_auction(auction: Auction) -> str:
    """Finish out an auction."""

    results = calculate_auction_results(
        user_id=auction.creator_id, auction_id=auction.id
    )

    message = [
        f"Auction on {auction.location} for {auction.amount} {auction.item}@{auction.price} {auction.currency}/u results:"
    ]
    for user_id, amount in results.items():
        user_name: str = await get_username(auction.guild_id, user_id)

        message.append(
            f"{user_name}: {amount} {auction.item} "
            f"for a total of {amount*auction.price} {auction.currency}"
        )

    result = "\n* ".join(message)

    unallocated_items = auction.amount - sum(results.values())
    del AUCTIONS[auction.id]
    save_auctions()
    return f"{result}\n\nUnallocated items: {unallocated_items} {auction.item}"
