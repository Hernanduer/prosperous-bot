# Prosperous Bot

This is a bot purpose built for helping a prosperous universe discord server.

[Invite me!](https://discord.com/api/oauth2/authorize?client_id=1084316099086127105&permissions=2048&scope=bot)


## The tools

This bot provides tools that help the server's member coordinate and collaborate with each other. All of the commands provided by the bot are slash commands so discord will assist with filling in and describing the parameters required.

### Auctions

If you have some resources you'd like to sell to the members of the server, but want a fair way to distribute them if multiple people are involved then you can use the `auction` commands provided by the bot.

#### Start
To start an auction, use the `auction start` command:
```
/auction start 5.5 10000 PE
```

This will start an auction to sell 10000 PE at 5.5 AIC per unit.


#### Bid
To bid on an auction, use the `auction bid` command:
```
/auction bid 12345
```

This will add your name to the bids for the auction with id `12345`.

If you don't want to potentially purchase the entire volume in the auction, you can set a maximum limit by adding the optional max amount parameter

```
/auction bid 12345 500
```

This will place a bid on auction `12345`, but cap your "winnings" to at most 500 units.


#### End

To end an auction you started use the `auction end` command:
```
/auction end 12345
```

This will end auction `12345` and display the results in the channel.

**Note**: Only the user that started the auction can end the auction.